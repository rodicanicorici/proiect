// Module dependencies.
var express = require("express")
var app     = express()
  , http    = require("http")
  , path    = require("path")
  , routes  = require("./routes")
  ,  MongoClient = require('mongodb').MongoClient
  ,  ObjectID = require('mongodb').ObjectID;
var config = require('./config')();


// All environments

app.set("port", 3000);
app.set("views", __dirname + "/views");
app.set("view engine", "ejs");
app.use(express.favicon());
app.use(express.logger("dev"));
app.use(express.bodyParser());
app.use(express.methodOverride());
app.use(express.cookieParser("61d333a8-6325-4506-96e7-a180035cc26f"));
app.use(express.session());
app.use(app.router);
app.use(express.static(path.join(__dirname, "public")));
app.use(express.errorHandler());
// App routes
app.get("/index"     , routes.index);

app.get("/shop/:category", routes.category);
app.get("/shop/:category/:subcategories", routes.subcategories);
app.get("/shop/:products", routes.products);
app.get("/shop/:products/:productsdetails", routes.productsdetails);

// Run server
http.createServer(app).listen(config.port, function(){
  console.log('Express server listening on port ' + config.port);
});
