var mdbClient = require('mongodb').MongoClient;
var _         = require("underscore");

function findById(categories, callback, query) {
	mdbClient.connect("mongodb://localhost:27017/pcat", function(err, db) {
		var collection = db.collection('categories');
		collection.find(query).toArray(function(err, items) {
			if (err) return callback(err, null);
			callback(err, items);
			db.close();
		});
	});
}




exports.index = function(req, res) {
	var _         = require("underscore");
	var mdbClient = require('mongodb').MongoClient;
	
	mdbClient.connect("mongodb://localhost:27017/pcat", function(err, db) {
		var collection = db.collection('categories');
		
		collection.find().toArray(function(err, items) {
			res.render("index", {
				// Underscore.js lib
				_     : _, 
				
				// Template data
				title : "Main category name",
				items : items
			});

			db.close();
		});
	});
};


exports.category = function(req, res) {
	var id = req.params.category;
	console.log(id);
	findById('categories', function(err, categories) {
		console.log(categories);
		var category = _.reduce(categories, function(elem) {
			return elem.categories;
		});
		console.log(category);
		console.log(category.categories);
		res.render("category", {
		_: _,
		title : id,
			items : categories,
			categories: category.categories,
			session : req.session
	});
}, {"id":id});
};


exports.subcategories = function(req, res) {
 var id = req.params.subcategories;
 console.log(id);
 findById('categories', function(err, categories) {
  console.log(categories);
  var category = _.reduce(categories, function(elem) {
   return elem.categories;
  });
  var filtrare = _.filter(category.categories, function(elem){ 
   return elem.id == id ; 
  });
  
  var subcategories = _.reduce(filtrare, function(elem) {
   return elem.categories;
  });
  
  
  res.render("subcategories", {
  _:_,
  title : id,
   items : category.categories,
   categories: category.categories,
   
   subcategories:subcategories.categories,
   session : req.session
 });
}, {"categories.id":id});
};




exports.products = function(req, res) {
 var id = req.params.products;
 console.log(id);
 findById('products', function(err, products) {
  console.log(products);
  var subcategories = _.reduce(products, function(elem) {
   return elem.products;
  });
  var filtrare = _.filter(subcategories.products, function(elem){ 
   return elem.id == id ; 
  });
  
  var products = _.reduce(filtrare, function(elem) {
   return elem.products;
  });
  
  
  res.render("products", {
  _:_,
  title : id,
   items : subcategories.products,
   products: subcategories.products,
   
   products:products.products,
   session : req.session
 });
}, {"products.id":id});
};

exports.productsdetails = function(req, res) {
 var id = req.params.products;
 console.log(id);
 findById('products', function(err, products) {
  console.log(products);
  var subcategories = _.reduce(products, function(elem) {
   return elem.products;
  });
  var filtrare = _.filter(products.productsdetails, function(elem){ 
   return elem.id == id ; 
  });
  
  var products = _.reduce(filtrare, function(elem) {
   return elem.productsdetails;
  });
  
  
  res.render("products", {
  _:_,
  title : id,
   items : products.productsdetails,
   products: products.productsdetails,
   
   products:products.productsdetails,
   session : req.session
 });
}, {"products.id":id});
};